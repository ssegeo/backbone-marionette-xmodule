/**
 * backbone-marionette-xmodule
 * Extend Backbone Marionette Module system to allows auto registering the current module's event handlers
 * 
 * @author : sqomp
 * @usage: in each module over module:eventsMap seting the various event => handlers
 * 		   pairs for the various bind categories
 * 		   
 * 		   Example:
 * 		   // create a new Marionette module
 * 		   APP.module('user',{
 * 		   		define: function(user){
 * 		   			// set up the eventsMap hash for this module
 * 		   			user.eventsMap = {
 * 		   				listenTo: {
 * 		   					//format 1
 * 		   					'dummy:event1': 'handler1',
 *
 * 							// format 2
 * 		   					'dummy:event2': 'handler1 handler2',
 *
 * 							// format 3
 * 		   					'dummy:event3': {src: APP.acl,context: user,callback: user.handler1},
 *
 * 							// format 4
 * 		   					'dummy:event3': {src: APP.acl,context: user,callback: 'handler1'},
 * 		   					'dummy:event4': {src: APP.acl,context: user,callback: 'handler1 handler2'},
 * 		   					'dummy:event5': {src: APP.acl,context: user,callback: [user.handler1,user.handler2]},
 *
 * 							// format 5
 * 		   					'dummy:event6': [
 * 		   										{src: APP.acl,context: user,callback: 'handler1'},
 * 		   										{src: APP.acl,context: user,callback: 'handler1 handler2'},
 * 		   										{src: APP.acl,context: user,callback: [user.handler1,user.handler2]}
 * 		   										{....}
 * 		   									],
 * 		   				},
 *
 * 						listenToOnce: {},
 * 						on: {},
 * 						once: {},
 * 		   			}
 *
 * 					user.handler1 = function(){};
 * 					user.handler2 = function(){};
 * 		   		}
 * 		   });
 */
_.extend(Marionette.Module.prototype,{
	start: _.wrap(Marionette.Module.prototype.start, function(oldStart){
		var args = Array.prototype.slice.call(arguments,1);

		if (!this._isInitialized) {
			// add custom event listeners
			this.once('start',this._registerListenToHandlers);
			this.once('start',this._registerListenToOnceHandlers);
			this.once('start',this._registerOnHandlers);
			this.once('start',this._registerOnceHandlers);				
		}

		// call the previous default initializer if we have any
		oldStart && oldStart.apply(this,args);
	}),

	/**
	 * Define an event map object to be used by the auto registers i.e _registerListenToHandlers,
	 * _registerListenToOnceHandlers, _registerOnHandlers, _registerOnceHandlers
	 * over this to define custom event object
	 * @type {Object}
	 */
	eventsMap: {},

	_registerListenToHandlers: function(){
		var self = this;

		_.each(this.eventsMap.listenTo,function(handler,event){
			handler = self._normalizeHandlers(handler);

			_.each(handler,function(_handler){
				var context = _handler.context || self;
				self._registerHandler.call(context,'listenTo',event,_handler.callback,_handler.src); 
			});
		});
	},

	_registerListenToOnceHandlers: function(){
		var self = this;

		_.each(this.eventsMap.listenToOnce,function(handler,event){
			handler = self._normalizeHandlers(handler);

			_.each(handler,function(_handler){
				var context = _handler.context || self;
				self._registerHandler.call(context,'listenToOnce',event,_handler.callback,_handler.src); 
			});
		});
	},

	_registerOnHandlers: function(){
		var self = this;

		_.each(this.eventsMap.on,function(handler,event){
			handler = self._normalizeHandlers(handler);

			_.each(handler,function(_handler){
				var context = _handler.context || self;
				self._registerHandler.call(context,'on',event,_handler.callback,_handler.src); 
			});
		});
	},

	_registerOnceHandlers: function(){
		var self = this;

		_.each(this.eventsMap.once,function(handler,event){
			handler = self._normalizeHandlers(handler);

			_.each(handler,function(_handler){
				var context = _handler.context || self;
				self._registerHandler.call(context,'once',event,_handler.callback,_handler.src); 
			});
		});
	},

	/**
	 * register event to the respective handler
	 * @param  {string} 		listenMethod Listening method to use when attaching handler
	 *                                     	i.e on,once,listenTo,listenToOnce
	 * @param  {string} 		event       event name to attach handler to
	 * @param  {array} 			handlers    an array of event handler(s)
	 * @param  {object|null} 	proxyObj    object responsible for triggering the event
	 *                                     	this argument is only relevant when listenTo &
	 *                                   	listenToOnce listen methods are being used
	 *                                   	bind the event(s)
	 * @return {null}              
	 */
	_registerHandler: function(listenMethod,event,handlers,proxyObj){
		var self = this;

		proxyObj = _.isObject(proxyObj) ? proxyObj : self;

		if (['listenTo','listenToOnce'].indexOf(listenMethod) >=0){
			// listeTo|listenToOnce
			_.each(handlers,function(handler){
				self[listenMethod](proxyObj,event,handler);
			});
		} else {
			// on|once
			_.each(handlers,function(handler){
				self[listenMethod](event,handler);
			});
		}
	},

	/**
	 * normalize different handler formats to one unison format 
	 * @param  {mixed} handler eventsMap key value
	 * @return {array}         array of normalized handlers
	 *                         [{src: obj,context: obj,callback:[Fn.handlers]}]
	 */
	_normalizeHandlers: function(handler){
		var self = this,
			ctx = this,
			handlers = [];

		if (!handler){
			return [{scr: this, callback: [],context: this}];
		}

		// working with 'handler1' or 'handler1 handler2'
		if (_.isString(handler)){
			return [{
				src: this, 
				context: this,
				callback: this._handlerFromString(handler,this)
			}];
		}

		// working with {src:'',callback: *}
		else if (_.has(handler,'callback')){

			// callback: string
			if (_.isString(handler.callback)){
				return [{
					src: _.result(handler,'src') || this,
					context: _.result(handler, 'context') || this,
					callback: this._handlerFromString(handler.callback)
				}];
			} 
			// callback: Array
			else if (_.isArray(handler.callback)){
				return [{
					src: _.result(handler, 'src') || this,
					context: _.result(handler, 'context') || this,
					callback: handler.callback
				}];
			} 

			// callback: Function
			else if (_.isFunction(handler.callback)){
				return [{
					src: _.result(handler, 'src') || this,
					context: _.result(handler, 'context') || this,
					callback: [handler.callback]
				}];
			} 

			// callback: invalid
			else {
				throw new APP.Error('_normalizeHandlers(): invalid callback');
			}
		}

		// working with [{src:'',callback: *},{...}]
		else if (_.isArray(handler)){
			var _handlers = [];
			// recurse each handler in the array
			_.each(handler,function(_handler){
				_handlers.push(self._normalizeHandlers(_handler));
			});

			// merge the handlers 
			return _.union.apply(this,_handlers);
		}
	},

	/**
	 * extracts handler(s) from strings
	 * @param  {string|array} handler handler string name |space seperated stringed handlers
	 *                                @formats: 1. 'handler'
	 *                                			2. 'handler1 handler2 handler3'
	 * @param  {object|null} ctx     handler's context to use defaults to this in case its
	 *                               not parsed
	 * @return {array}         array of handler function objects
	 */
	_handlerFromString: function(handler,ctx){
		var handlers = [],
			spliter = /\s+/;

		ctx = _.isObject(ctx) ? ctx : this;

		if ('string' !== typeof handler ){
			throw new APP.Error('_handlerFromString(): expects a string but has ['+typeof handler+']');				
		}

		// split handler into an array of handler strings(keys)
		handler = handler.split(spliter);

		// loop thorought the handler array of string constructing their respect 
		// function objects with respect to the context object (ctx)
		_.each(handler,function(_handler){
			handlers.push(ctx[_handler]);
		});

		return handlers;
	}
});