# backbone-marionette-xmodule #
Extend Backbone Marionette Module system to allows auto registering the current module's event handlers

@author : sqomp [code-256]

@usage: in each module override module:eventsMap seting the various event => handlers
		   pairs for the various bind categories
		   
		   Example:
		   // create a new Marionette module
		   APP.module('user',{
		   		define: function(user){
		   			// set up the eventsMap hash for this module
		   			user.eventsMap = {
		   				listenTo: {
		   					//format 1
		   					'dummy:event1': 'handler1',

							// format 2
		   					'dummy:event2': 'handler1 handler2',

							// format 3
		   					'dummy:event3': {src: APP.acl,context: user,callback: user.handler1},

							// format 4
		   					'dummy:event3': {src: APP.acl,context: user,callback: 'handler1'},
		   					'dummy:event4': {src: APP.acl,context: user,callback: 'handler1 handler2'},
		   					'dummy:event5': {src: APP.acl,context: user,callback: [user.handler1,user.handler2]},

							// format 5
		   					'dummy:event6': [
		   										{src: APP.acl,context: user,callback: 'handler1'},
		   										{src: APP.acl,context: user,callback: 'handler1 handler2'},
		   										{src: APP.acl,context: user,callback: [user.handler1,user.handler2]}
		   										{....}
		   									],
		   				},

						listenToOnce: {},
						on: {},
						once: {},
		   			}

					user.handler1 = function(){};
					user.handler2 = function(){};
		   		}
		   });