/* global describe, it */
var APP = new Marionette.Application();
APP.start();

(function () {
    'use strict';
	describe("marionette-xmodule", function() {
		APP.module('dummy',{
				initialize: function(){},
				define: function(dummy){
					dummy.dummyHandler = function(){
						console.log('Dummy Handler');
					};
				},
				startWithParent: false
			}); 


		function eventsMap(){
			APP.dummy.eventsMap = {
				listenTo: {
					'event': [{
						src: '',
						context: this,
						callback: Function.prototype,
					}]
				},

				listenToOnce: {
					'event': [{
						src: '',
						context: this,
						callback: Function.prototype
					}]
				},

				on: {
					'event':[{
						context: this,
						callback: Function.prototype
					}]
				},

				once: {
					'event':[{
						context: this,
						callback: Function.prototype
					}]
				}    					
			};
		}

		function resetDummyModule(){
			APP.dummy.eventsMap = {},

			APP.dummy.off();

			APP.dummy.stop();
		}

		before(function () {
		});

		after(function(){
			APP.dummy.stop();
		});

    	it ('Auto registers event listeners defined in module::eventsMap',function(done){
    		var callbacks = ['_registerListenToHandlers','_registerListenToOnceHandlers','_registerOnHandlers','_registerOnceHandlers'],
    			spies = {};

			_.each(callbacks,function(cb){
				spies[cb] = sinon.spy(APP.dummy,cb);
			});

			APP.dummy.start();

			_.each(callbacks,function(cb){
				expect(spies[cb]).not.to.have.been.calledOn(APP.module);

				expect(spies[cb]).to.have.been.calledOn(APP.dummy);

				expect(spies[cb]).to.have.been.calledOnce;

				// reset the spies after evaluating it
				spies[cb].reset();
			});

			// ensure it only auto registers them once per module start
			APP.dummy.start();

			_.each(callbacks,function(cb){
				expect(spies[cb]).not.to.have.been.called;
			});	

			done();		
    	});

    	it ('defines module::eventsMap',function(){
    		expect(APP.dummy.eventsMap).to.be.ok;
    	});

    	
    	describe('_normalizeHandlers()',function(){    
			_.extend(APP.dummy,{handler1: function(){},handler2:function(){}});

    		var format1 = 'handler1',
    			format2 = 'handler1 handler2',
    			format3 = {src: APP.dummy,callback:APP.dummy.handler1,context:APP.dummy},
    			format4 = {callback:APP.dummy.handler1,context:APP.dummy},
    			format5 = {callback:APP.dummy.handler1},
    			format6 = [
    						{src: APP.dummy,callback:APP.dummy.handler1,context:APP.dummy},
    						{src: APP.dummy,callback:APP.dummy.handler2,context:APP.dummy},
    					  ],
				format7 = {src: APP.dummy,callback:'handler1',context: APP.dummy},
				format8 = {src: APP.dummy,callback:'handler1 handler2',context: APP.dummy},
				format9 = {src: APP.dummy,callback:[APP.dummy.handler1,APP.dummy.handler2],context: APP.dummy};

			resetDummyModule();

			it("should normalize string handlers", function (done) {
				expect(APP.dummy._normalizeHandlers(format1)).deep.to.be.equal([{
					src:APP.dummy,
					callback:[APP.dummy.handler1],
					context:APP.dummy
				}]);

				expect(APP.dummy._normalizeHandlers(format2)).deep.to.be.equal([{
					src:APP.dummy,
					callback:[APP.dummy.handler1,APP.dummy.handler2],
					context:APP.dummy
				}]);

				done();
			});

			it('should normalize object handlers',function (done){
				var result = [{
					src:APP.dummy,
					callback:[APP.dummy.handler1],
					context:APP.dummy
				}];

				expect(APP.dummy._normalizeHandlers(format3)).deep.to.be.equal(result);
				expect(APP.dummy._normalizeHandlers(format4)).deep.to.be.equal(result);
				expect(APP.dummy._normalizeHandlers(format5)).deep.to.be.equal(result);

				done();
			});

			it('should normalize array handlers', function (done) {
				var result = [{
					src:APP.dummy,
					callback:[APP.dummy.handler1],
					context:APP.dummy
				},{
					src:APP.dummy,
					callback:[APP.dummy.handler2],
					context:APP.dummy
				}];

				expect(APP.dummy._normalizeHandlers(format6)).deep.to.be.equal(result);

				done();
			});

			it('should normalize object handler with callback specified as a string', function(done) {
				var result = [{
						src:APP.dummy,
						callback:[APP.dummy.handler1],
						context:APP.dummy
					}],
					result2 = [{
						src:APP.dummy,
						callback:[APP.dummy.handler1,APP.dummy.handler2],
						context:APP.dummy
					}];

				expect(APP.dummy._normalizeHandlers(format7)).deep.to.be.equal(result);
				expect(APP.dummy._normalizeHandlers(format8)).deep.to.be.equal(result2);

				done();
			});

			it('should normalize object handler with callback specified as an array', function(done) {
				var result = [{
						src:APP.dummy,
						callback:[APP.dummy.handler1,APP.dummy.handler2],
						context:APP.dummy
					}];

				expect(APP.dummy._normalizeHandlers(format9)).deep.to.be.equal(result);

				done();
			});
    	});

    	describe('_handlerFromString()', function () {
    		var ctx = 	{	
    						handler1: function(){},
    						handler2: function(){}
					  	},
    					format1 = 'handler1',
    					format2 = 'handler1 handler2',
    					format3 = ['handler1','handler2'],
    					handlers = [];

			it ('should handle singular string handlers',function(){
				handlers = APP.dummy._handlerFromString(format1,ctx);
				expect(handlers).deep.to.be.equal([ctx.handler1]); 				
			});

			it('should handle spaced handlers', function() {
				handlers = APP.dummy._handlerFromString(format2,ctx);
				expect(handlers).deep.to.be.equal([ctx.handler1,ctx.handler2]);
			});

			it('should throw on non string handlers', function () {
				expect(APP.dummy._handlerFromString.bind(APP.dummy,{},ctx)).to.throw(APP.Error);
				expect(APP.dummy._handlerFromString.bind(APP.dummy,format3,ctx)).to.throw(APP.Error);
			});
    	});

    	describe('_registerHandler()', function () {
    		beforeEach(function () {
    			resetDummyModule();
    		});


    		afterEach(function () {
    			try {
    				APP.dummy._registerHandler.restore();
    			}catch(e){}

    			resetDummyModule();
    		});

    		it('Should Handle actual registration of module event hanlders',function(){
    			var registerHandlerSpy = sinon.spy(APP.dummy,'_registerHandler');
				
    			APP.dummy.eventsMap = {
    				listenTo: {
    					'dummy:event': {
    						src: APP.dummy,
    						callback: APP.dummy.dummyHandler,
    						context: APP.dummy
    					}
    				}
    			};

    			APP.dummy.start();

    			expect(registerHandlerSpy).to.have.been.calledOn(APP.dummy);

    			expect(registerHandlerSpy).to.have.been.calledWith('listenTo','dummy:event',[APP.dummy.dummyHandler],APP.dummy);
    		});

    		it('Should register multiple events',function(){
    			var dummyHandlerSpy = sinon.spy();

    			APP.dummy.eventsMap = {
    				listenTo: {
    					'dummy:event dummy:event2': {
    						src: APP.dummy,
    						callback: dummyHandlerSpy,
    						context: APP.dummy
    					}
    				}
    			};  

    			var registerHandlerSpy = sinon.spy(APP.dummy,'_registerHandler');

    			APP.dummy.start();

    			expect(registerHandlerSpy).to.have.been.called;

    			APP.dummy.trigger('dummy:event');
    			expect(dummyHandlerSpy).to.have.been.calledOnce;

    			APP.dummy.trigger('dummy:event2');
    			expect(dummyHandlerSpy).to.have.been.calledTwice;
    		});

    		it('Should register multiple handlers',function(){
    			var dummyHandlerSpy1 = sinon.spy();
    			var dummyHandlerSpy2 = sinon.spy();

    			APP.dummy.eventsMap = {
    				listenTo: {
    					'dummy:event': [{
    						src: APP.dummy,
    						callback: dummyHandlerSpy1,
    						context: APP.dummy
    					},{
    						src: APP.dummy,
    						callback: dummyHandlerSpy2,
    						context: APP.dummy
    					}]
    				}
    			};  

    			var registerHandlerSpy = sinon.spy(APP.dummy,'_registerHandler');

    			APP.dummy.start();

    			expect(registerHandlerSpy).to.have.been.called;

    			APP.dummy.trigger('dummy:event');
    			expect(dummyHandlerSpy1).to.have.been.calledOnce;
    			expect(dummyHandlerSpy2).to.have.been.calledOnce;
    		});
    	});

    	describe('_registerListenToHandlers()', function () {
    		beforeEach(function () {
    			resetDummyModule();
    		});

    		afterEach(function () {
    			resetDummyModule();
    		});

    		it('Should register listenTo Handlers', function () {
    			var dummyHandlerSpy = sinon.spy();

    			APP.dummy.eventsMap = {
    				listenTo: {
    					'dummy:event': {
    						src: APP.dummy,
    						callback: dummyHandlerSpy,
    						context: APP.dummy
    					}
    				}
    			};

    			APP.dummy.start();

    			APP.dummy.trigger('dummy:event');

    			expect(dummyHandlerSpy).to.have.been.called;

    			APP.dummy.trigger('dummy:event');

    			expect(dummyHandlerSpy).to.have.been.calledTwice;
    		});
    	});

    	describe('_registerListenToOnceHandlers()', function () {
    		beforeEach(function () {
    			resetDummyModule();
    		});

    		afterEach(function () {
    			resetDummyModule();
    		});

    		it('Should register listenTo Handlers', function () {
    			var dummyHandlerSpy = sinon.spy();

    			APP.dummy.eventsMap = {
    				listenToOnce: {
    					'dummy:event': {
    						src: APP.dummy,
    						callback: dummyHandlerSpy,
    						context: APP.dummy
    					}
    				}
    			};

    			APP.dummy.start();

    			APP.dummy.trigger('dummy:event');

    			expect(dummyHandlerSpy).to.have.been.called;

    			APP.dummy.trigger('dummy:event');

    			expect(dummyHandlerSpy).to.have.been.calledOnce;
    		});
    	});    	

    	describe('_registerOnHandlers()', function () {
    		beforeEach(function () {
    			resetDummyModule();
    		});

    		afterEach(function () {
    			resetDummyModule();
    		});

    		it('Should register listenTo Handlers', function () {
    			var dummyHandlerSpy = sinon.spy();

    			APP.dummy.eventsMap = {
    				on: {
    					'dummy:event': {
    						src: APP.dummy,
    						callback: dummyHandlerSpy,
    						context: APP.dummy
    					}
    				}
    			};

    			APP.dummy.start();

    			APP.dummy.trigger('dummy:event');

    			expect(dummyHandlerSpy).to.have.been.called;

    			APP.dummy.trigger('dummy:event');

    			expect(dummyHandlerSpy).to.have.been.calledTwice;
    		});
    	});

    	describe('_registerOnceHandlers()', function () {
    		beforeEach(function () {
    			resetDummyModule();
    		});

    		afterEach(function () {
    			resetDummyModule();
    		});

    		it('Should register listenTo Handlers', function () {
    			var dummyHandlerSpy = sinon.spy();

    			APP.dummy.eventsMap = {
    				once: {
    					'dummy:event': {
    						src: APP.dummy,
    						callback: dummyHandlerSpy,
    						context: APP.dummy
    					}
    				}
    			};

    			APP.dummy.start();

    			APP.dummy.trigger('dummy:event');

    			expect(dummyHandlerSpy).to.have.been.called;

    			APP.dummy.trigger('dummy:event');

    			expect(dummyHandlerSpy).to.have.been.calledOnce;
    		});
    	});     		
	});
})();